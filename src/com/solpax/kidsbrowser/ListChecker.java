package com.solpax.kidsbrowser;

import java.net.URI;
import java.util.StringTokenizer;

import android.webkit.WebView;

public class ListChecker {
	//test against www.google.com.ru/somespammysite
	//urlList format host path
	//implement path part later
	//returns false for blocked urls, true for okay urls
	public static boolean CheckUrlAgainstList(String url, String[] urlList){
		if(urlList == null || urlList.length < 1){
			return false;
		}
		boolean urlOkay = false;
		URI uri = URI.create(url);
		String host = uri.getHost().toLowerCase();
		//String path = uri.getPath();
		//String query = uri.getQuery();//convert to lower case if not null
		for(int i = 0; i< urlList.length; i++){
			String check = urlList[i].toLowerCase();
			boolean blockEntry =false;
			if(check.startsWith("^")){
				blockEntry=true;
				check = check.substring(1);
			}
			//only check if not already checked, or if checking for negative
			if(!urlOkay || blockEntry){
				//escape dots... no other special chars?
				String checkRegex = check.replace(".", "\\.");
				checkRegex = "^"+checkRegex.replace("*", "\\w*")+"$";
				boolean matched = host.matches(checkRegex);
				if(blockEntry && matched){
					urlOkay = false;
				}else{
					urlOkay=matched;
				}
			}
		}
		return urlOkay;
	}

	public static boolean CheckViewHasBadWords(WebView view, String[] wordList){
		//returns false if view is okay, true if not
		if(wordList == null){
			return false;
		}
		for(int i=0; i<wordList.length; ++i){
			String[] wordParts = wordList[i].split("\\t");
			String searchWord = wordParts[0];
			int limit = 0;
			if(wordParts.length > 1){
				String limitStr = wordParts[1];
				try{
					limit = Integer.parseInt(limitStr);
				}catch(Exception x){}
			}
			int matches = view.findAll(searchWord);
			if(matches > limit){
				return true;
			}
		}
		return false;
	}
	
	public static boolean CheckUrlContainsBadWords(String url, String[] wordList){
		//returns false if url is okay, true if it contains bad words
		if(wordList == null){
			return false;
		}
		String lurl = url.toLowerCase();
		lurl = lurl.replaceAll("[^a-zA-Z]", " ");
		for(int i=0; i<wordList.length; ++i){
			String[] wordParts = wordList[i].split("\\t");
			String searchWord = wordParts[0];
			int limit = 0;
			if(wordParts.length > 1){
				String limitStr = wordParts[1];
				try{
					limit = Integer.parseInt(limitStr);
				}catch(Exception x){}
			}
			String searchLurl = lurl;
			//count the number of matches
			int matches =0;
			int wordIdx = searchLurl.indexOf(searchWord);
			while(wordIdx >= 0){
				matches++;
				if(searchLurl.length()> (searchWord.length() + wordIdx)){
					searchLurl = searchLurl.substring(wordIdx + searchWord.length());
					wordIdx = searchLurl.indexOf(searchWord);
				}
			}
			if(matches > limit) return true;
		}
		return false;
	}
}
