package com.solpax.kidsbrowser;

import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class KidsBrowserActivity extends Activity {
	BrowserWebView kWebView;
	BrowserWebViewClient kWebViewClient;
	WebViewUpdateCallback clientUpdateCallback;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.main);
        setProgressBarIndeterminateVisibility(true);
        setProgressBarVisibility(true);
        setupCallback();
        setUpEditText();
       	setupWebView(savedInstanceState);
        Uri data = this.getIntent().getData();
        if(data != null){
	        URL url = null;
	        try {
				url = new URL(data.getScheme(), data.getHost(), data.getPath());
				loadUrl(url.toString());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception x){
				x.printStackTrace();
			}
        }
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.SyncOpt:
        	setProgress(30 * 100);
        	BrowserListUtility.syncWordList(this);
        	setProgress(60 * 100);
        	BrowserListUtility.syncUrlList(this);
        	setProgressBarIndeterminateVisibility(false);
            setProgressBarVisibility(false);
        	Toast toast = Toast.makeText(this, "Done",Toast.LENGTH_SHORT);
        	toast.show();
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    private void setupWebView(Bundle savedInstanceState){
    	kWebView = (BrowserWebView) findViewById(R.id.webview);
        //enable javascript, etc.
        kWebView.getSettings().setJavaScriptEnabled(true);
        kWebView.getSettings().setJavaScriptEnabled(true); 
        kWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        kWebView.getSettings().setBuiltInZoomControls(true);
        //kWebView.setInitialScale(75);
        //use custom webclient
        kWebViewClient = new BrowserWebViewClient(kWebView.getContext());
        kWebViewClient.clientUpdateCallback = clientUpdateCallback;
        kWebView.setWebViewClient(kWebViewClient);
        kWebView.setUpdateCallback(clientUpdateCallback);
        kWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                setProgress(progress * 100);
               if(progress == 100) {
                  setProgressBarIndeterminateVisibility(false);
                  setProgressBarVisibility(false);
               }
            }
         });
        
    	if(savedInstanceState != null){
    		kWebView.restoreState(savedInstanceState);
    		final EditText urlField = (EditText) findViewById(R.id.locationEditText);
    		urlField.setText( kWebView.getUrl());
    	}
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	if(kWebView != null) kWebView.saveState(outState);
     }
    
    //creates anonymous callback
    private void setupCallback(){
    	clientUpdateCallback = new WebViewUpdateCallback(){
    		private boolean toggleLocBar = true;
			@Override
			public void urlLoaded(String url) {
				SetUrlField(url);
			}
			@Override
			public void scrollChanged(int left, int top, int oldleft, int oldtop){
				//hide the location bar
				if(!toggleLocBar){
					toggleLocBar = true;
					return;
				}
				final EditText urlField = (EditText) findViewById(R.id.locationEditText);
				final LinearLayout locationBar = (LinearLayout)urlField.getParent();
				int locH = locationBar.getHeight();
				int diff = oldtop - top;
				//Log.d("scroll", "top:"+String.valueOf(top) + " oldTop:"+String.valueOf(oldtop)+" diff:"+String.valueOf(diff));
				if(top<2){
					if(locH < 72){
						toggleLocBar = false;
						locationBar.getLayoutParams().height =72; 
						locationBar.requestLayout();
						kWebView.scrollTo(left, 0);
					}
				}else{
					if(oldtop > 0 && locH > 0 && top > 75){
						locationBar.getLayoutParams().height =0; 
						locationBar.requestLayout();
					}
				}
			}
    	};
    }
    
    public void loadURL(View button) {
    	final EditText urlField = (EditText) findViewById(R.id.locationEditText);
    	String url = urlField.getText().toString();
    	urlField.clearFocus();
    	loadUrl(url);
    }
    
    private void loadUrl(String url){
    	if(!url.contains(":")){
    		url = "http://" + url;
    		SetUrlField(url);
    	}
    	if(kWebViewClient.checkURLisOk(url)){
    		//blank it until the contents are checked
    		//will be turned back on by onPageFinished event
    		kWebView.setVisibility(View.INVISIBLE);
    		kWebView.loadUrl(url);
    	}else{
    		kWebViewClient.HandleBlockedPage(kWebView, "that website is not allowed");
    	}
    }
    
    private void setUpEditText(){
    	final EditText urlField = (EditText) findViewById(R.id.locationEditText);
    	urlField.setOnKeyListener(new OnKeyListener() {           
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction()==KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                	String url = urlField.getText().toString();
                    loadUrl(url);
                    urlField.clearFocus();           
                	return true;
                }
                return false;
            }
        });
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && kWebView.canGoBack()) {
            kWebView.goBack();
            final EditText urlField = (EditText) findViewById(R.id.locationEditText);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    private void SetUrlField(String url){
    	final EditText urlField = (EditText) findViewById(R.id.locationEditText);
    	urlField.setText(url);
    	urlField.clearFocus();
    }
}
