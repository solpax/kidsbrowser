package com.solpax.kidsbrowser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;


public class BrowserListUtility {
	public static String[] UrlList;
	public static String[] WordList;
	private Context callingContext;
	
	public BrowserListUtility(Context context){
		callingContext = context;
		String wordFile = context.getString(R.string.localWordFile);
		String urlFile = context.getString(R.string.localUrlFile);
		UrlList = ReadTextFile(urlFile);
		WordList = ReadTextFile(wordFile);
	}
	
	public static String[] syncWordList(Context context){
		String server = context.getString(R.string.appServer);
		String path = context.getString(R.string.wordListPath);
		WordList = fetchRemoteFile("http://"+server+"/"+path);
		String wordFile = context.getString(R.string.localWordFile);
		SaveTextFile(context, wordFile,WordList);
		return WordList;
	}
	
	public static String[] syncUrlList(Context context){
		String server = context.getString(R.string.appServer);
		String path = context.getString(R.string.urlListPath);
		UrlList = fetchRemoteFile("http://"+server+"/"+path);
		String urlFile = context.getString(R.string.localUrlFile);
		SaveTextFile(context, urlFile,UrlList);
		return UrlList;
	}
	
	private static String[] fetchRemoteFile(String url){
	    HttpClient client = new DefaultHttpClient();
	    HttpGet request = new HttpGet(url);
	    try {
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
		    String[] fileContents= readStringStream(in);
		    in.close();
		    return fileContents;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String[] readStringStream(InputStream in) throws IOException {
		String line = null;
		ArrayList<String> stringList = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder str = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			stringList.add(line);
		}
		
		return stringList.toArray(new String[stringList.size()]);
	}
	
	private String[] ReadTextFile(String fileName){
		try {
			FileInputStream fis = callingContext.openFileInput(fileName);
			String[] fileContents = readStringStream(fis);
			fis.close();
			return fileContents;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static void SaveTextFile(Context context,String fileName, String[] contents){
		try {
			FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			for(int i =0; i< contents.length; i++){
				fos.write(contents[i].getBytes());
				fos.write("\n".getBytes());
			}
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
