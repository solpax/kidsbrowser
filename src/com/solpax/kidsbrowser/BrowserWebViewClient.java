package com.solpax.kidsbrowser;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class BrowserWebViewClient extends WebViewClient {

	public WebViewUpdateCallback clientUpdateCallback=null;
	
	private BrowserListUtility listUtility;
	private Context curContext;
	
	public BrowserWebViewClient(Context context){
		super();
		this.listUtility = new BrowserListUtility(context);
		this.curContext = context;
	}
	
	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String reqUrl) {
        if(reqUrl == null) return true;
        String url = reqUrl.toLowerCase();//necessary?
        if(url.startsWith("http:") || url.startsWith("https:")){
        	//check the url
        	boolean isOkay = checkURLisOk(url);
        	//update the GUI
        	if(isOkay){
	        	view.setVisibility(View.INVISIBLE);
	            view.loadUrl(reqUrl);
        	}else{
        		view.loadData("<html><body>that website is not allowed</body></html>", "text/html", null);
        	}
            return true;
        } else if (url.startsWith("tel:")) {
        	view.getContext().startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(reqUrl)));
            return true;
        } else if (url.startsWith("mailto:")) {
            url = url.replaceFirst("mailto:", "");
            url = url.trim();
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text").putExtra(Intent.EXTRA_EMAIL, new String[]{reqUrl});
            view.getContext().startActivity(i);
            return true;
        } else if (url.startsWith("geo:")) {
        	//handles geo??
            return true;
        }else if (url.startsWith("market://")) {
            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(reqUrl)));
            return true;
        } else {
        	//not sure what this actually does...
        	//return false;
        	Toast msg = Toast.makeText(view.getContext(), "Mystery URL! " + reqUrl, Toast.LENGTH_LONG);
        	msg.show();
        	view.loadUrl(reqUrl);
        	return true;
        }
    }

	public boolean checkURLisOk(String url) {
		boolean isOkay = false;
		try{
			isOkay = ListChecker.CheckUrlAgainstList(url, listUtility.UrlList);
		}catch(Exception x){
			Toast.makeText(curContext, x.getMessage(), Toast.LENGTH_LONG);
		}
		return isOkay;
	}
	
    @Override
    public void onLoadResource(WebView  view, String  url){
    	//TODO: need plan for "words" appearing in a string, like "sussex"
		if(ListChecker.CheckUrlContainsBadWords(url, listUtility.WordList)){
			HandleBlockedPage(view,"blocked for bad words");
		}
    	super.onLoadResource(view, url);
    }
    
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
		if(view.getVisibility() != View.INVISIBLE){
    		view.setVisibility(View.INVISIBLE);
    	}
        super.onPageStarted(view, url, favicon);
    }
    
    @Override
    public void onPageFinished(WebView view, String url) {
    	if(clientUpdateCallback != null){
    		clientUpdateCallback.urlLoaded(url);
    	}
    	boolean pageBad = ListChecker.CheckViewHasBadWords(view, listUtility.WordList);
    	if(pageBad){
    		HandleBlockedPage(view,"blocked for bad words");
    	}
    	if(view.getVisibility() != View.VISIBLE){
    		view.setVisibility(View.VISIBLE);
    	}
    	super.onPageFinished(view, url);
    }
    
    public void HandleBlockedPage(WebView view, String msg){
    	//Toast toast = Toast.makeText(view.getContext(), msg, Toast.LENGTH_LONG);
    	//toast.show();
    	//alternate
		view.stopLoading();
    	view.clearView();
		view.loadData("<html><body>"+msg+"</body></html>", "text/html", null);
    }
    
}
