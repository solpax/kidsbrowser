package com.solpax.kidsbrowser;

public interface WebViewUpdateCallback {
	public void urlLoaded(String url);
	public void scrollChanged(int left, int top, int oldleft, int oldtop);
}
