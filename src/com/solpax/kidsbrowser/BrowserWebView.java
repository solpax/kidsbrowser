package com.solpax.kidsbrowser;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class BrowserWebView extends WebView {
	private WebViewUpdateCallback updateCallback;
	public BrowserWebView(Context context){
		super(context);
	}
	public BrowserWebView(Context context, AttributeSet attributes) {
        super(context, attributes);
	}
	
	public void setUpdateCallback(WebViewUpdateCallback callback){
		updateCallback = callback;
	}
	
	@Override
	public void onScrollChanged(int left, int top, int oldleft, int oldtop) {
		if(updateCallback != null){
			updateCallback.scrollChanged(left,top,oldleft,oldtop);
		}
	}
}
